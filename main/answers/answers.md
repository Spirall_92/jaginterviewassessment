#JAG Method software developer assessment
## Answers

### 1. SEO (5min)

1) Title Tag Index.cshtml file needs to be an accurate and concise description of a page's content,

2) Description Meta Tag in the Index.cshtml file also needs to be an accurate and concise description of a page's content.

3) keywords Meta Tag should contain words separate by comma that relate to the Web pages core business' jargon.

4) The HTML Image Tags in the Index.cshtml file are missing "alt" attributes which describes what the image is about.


### 2. Responsive (15m)

1) Columns e.g "col-md-9" CSS classes are used without row class in the parent element (added row class).

2) add here

3) add here

4) add here


### 3. Validation (15m)

 <script>

        var $form = $("#myform");
        var $submitbutton = $("#submitbutton");

        $form.on("blur", "input", () => {
            if ($form.valid()) {
                $submitbutton.removeAttr("disabled");
            } else {
                $submitbutton.attr("disabled", "disabled");
            }
        });


 </script>

 <Script type="text/javascript">
        $(document).click("orange-button")
    </Script>


    <script type="text/javascript">
        // call ASP.NET MVC Controller 
        function SerializeMappings() {
            var matches = [];

            $('[class^="mappings_"]:checked').each(function () {
                matches.push(this.value);
            });

            $.post("/Home/Validate", { listofmappings: matches }, function (data) {
                document.write(data);
            });
        }
    </script>

### 4. JavaScript (20m)

@section Scripts{
    <script type="text/javascript">
        $(document).ready(function () {
            $('#lnkLead').addClass('activeMenu');

            var servedValue = 6000000;
            $('#served_value').text(servedValue);

            setInterval(function () {
                var randomValue = Math.floor((Math.random() * 20) + 1);
                servedValue += randomValue;
                $('#served_value').text(servedValue);
            }, 3000);

        });
    </script>


### 5. Ajax calls (30m)

 @*using (Html.BeginForm("SubmitLead", "Lead", FormMethod.Post, new { id = "frmsubmit" }))*@

                @using (Ajax.BeginForm("SubmitLead", "Lead", null, new AjaxOptions
                {
                    HttpMethod = "POST",
                    InsertionMode = InsertionMode.Replace,
                    UpdateTargetId = "FormOuter"
                }))
                {

### 6. Call a REST webservice (25m)
Add any special implemetation instructions here.

Make sure that the WebHost calls the ServiceHost via REST.

### 7. ADO.Net (40m)

create table Lead
(
Leadid varchar(50) not null Auto_Increment,
TrackingCode int,
FirstName varchar(255),
LastName varchar(255),
ContactNumber varchar(20),
Email varchar(50),
ReceivedDateTime date,
IPAddress varchar(15),
UserAgent varchar(30),
ReferrerURL varchar(100),
IsDuplicate bit default 0,
IsCapped bit default 0,
IsSuccessful bit default 0
PRIMARY KEY (Leadid)
)

Create table LeadParameter
(
LeadParameterId varchar(50),
Leadid varchar(50),
Name varchar(255),
Value varchar(255)
PRIMARY KEY (LeadParameterId)
FOREIGN KEY (Leadid) REFERENCES Lead(Leadid)
)

### 8. Poll DB (15m)
Add any SQL schema changes here

Make changes ServiceHost

### 9. SignalR (40m)
Add any SQL schema changes here

### 10. Data Analysis (30m)

1) Total Profit

select Earnings, cost, (Earnings - Cost)  Profit
from [dbo].[LeadDetail]
where (cost is not null and cost  <> 0)
	and  (Earnings is not null and Earnings  <> 0)


2) Total Profit (Earnings less VAT)

declare @VAT float
set @VAT =  0.15

select Earnings, (Earnings + @VAT) VATEarnings , Cost 
	 ,  ((Earnings + @VAT) - Cost)  Profit2
from [dbo].[LeadDetail]
where (cost is not null and cost  <> 0)
	   and  (Earnings is not null and Earnings  <> 0)

3) Profitable campaigns

declare @VAT float
set @VAT =  0.15

select distinct
	 (Earnings + @VAT) VATEarnings 
	 ,Cost
	 ,convert(decimal(10,2),((Earnings + @VAT) - Cost))  Profit2
	 ,c.ClientName, cp.CampaignName
from [LeadDetail] l
	inner join [Client] c on l.Clientid = c.Clientid
	inner join [Campaign] cp on l.campaignid = cp.campaignid
	
where (cost is not null and cost  <> 0)
  and (Earnings is not null and Earnings  <> 0)
  and ((Earnings + @VAT) - Cost) > 0


4) Average conversion rate

select sum(Earnings) Sum_Earnings
from [LeadDetail]
where Issold = 1 

--  Sum_Earnings_IsSold = 46003.67265978046782


select sum(Earnings)/ Sum_Earnings
from [LeadDetail]
where IsAccepted = 1 

-- Sum_Earnings_Accepted = 93177.00659027127741

Declare @Sales float  = 46003.67265978046782
Declare @Accepted float  = 93177.00659027127741

select (@Sales / @Accepted)*100 [Conversion rate %]

5) Pick 2 clients based on Profit & Conversion rate & Why?
**Answer**

**SQL**
`Select....`
